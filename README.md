# Cloud-Init Configurations for VMs and CTs

This repository contains cloud-init configurations to automate the provisioning of virtual machines (VMs) and containers (CTs). The configurations are designed to work with Ubuntu OS and can be used in environments like Proxmox Virtual Environment (PVE).

## Overview

- `base.yaml`: Base configuration that creates an initial user `paul`.
- `vm.yaml`: VM-specific configuration that includes the base configuration and upgrades the system.
- `ct.yaml`: CT-specific configuration that includes the base configuration and installs the Docker daemon.

## Usage

### For Virtual Machines

1. Use the `vm.yaml` configuration when creating a new VM.
2. Ensure that the VM's OS is compatible with cloud-init (e.g., Ubuntu).
3. Provide the cloud-init configuration during the VM creation process.

### For Containers

1. Use the `ct.yaml` configuration when creating a new container.
2. Ensure that the container's OS is compatible with cloud-init (e.g., Ubuntu).
3. Provide the cloud-init configuration during the container creation process.

## Customization

You can customize these configurations by editing the YAML files. Refer to the [cloud-init documentation](https://cloudinit.readthedocs.io/) for more details on the available options and syntax.

## Security Considerations

If you need to include sensitive information like SSH keys, ensure that they are handled securely and that access to the configurations is appropriately restricted.

## Contributing

Feel free to contribute to this project by submitting pull requests or reporting issues.

## License

This project is licensed under the MIT License. See the [LICENSE](LICENSE) file for details.
